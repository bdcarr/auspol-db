'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PolicySchema = new Schema({
    shortDescription: String,
    longDescription: String,
    categories: [{
        type: String,
        enum: ['Tax', 'Foreign Affairs', 'Trade', 'Governance', 'Environment', 'Education', 'Health',
                'Justice', 'Infrastructure', 'Immigration', 'Social'] // TODO: Look at moving these to a config file
    }],
    citations: [{
        link: String,
        title: String
    }],
    rating: {
        type: String,
        enum: ['Very Good', 'Good', 'Neutral', 'Bad', 'Very Bad']
    },
    meta: {
        created_date: {
            type: Date,
            default: Date.now
        }
    }
});

module.exports = mongoose.model('Policy', PolicySchema);