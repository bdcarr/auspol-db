'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var PolicyModel = require('./policyModel');

var PollieSchema = new Schema({
  name: {
    first: String,
    middle: String,
    last: String
  },
  dob: Date,
  parties: [{
    party: { type: Schema.Types.ObjectId, ref: "Party" },
    dateJoined: Date,
    dateLeft: Date
  }],
  policies: [PolicyModel.schema],
  meta: {
    created_date: {
      type: Date,
      default: Date.now
    }
  }
});

PollieSchema.virtual('currentParty').get(function() {
  if (this.parties != null) {
    this.parties.forEach(party => {
      if (party.dateLeft == null) {
        return party.party;
      }
    });
  }
  // by this point either there are no parties in their list, or they've left them all
  return "Independent";
});

module.exports = mongoose.model('Pollie', PollieSchema);