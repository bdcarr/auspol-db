'use strict'
var mongoose = require('mongoose')
var Schema = mongoose.Schema
var PolicyModel = require('./policyModel')

// TODO: Define the prototype party schema
var PartySchema = new Schema({
  names: [{
    name: String,
    required: true,
    startDate: Date
  }],
  description: String,
  policies: [PolicyModel.schema],
  startDate: Date,
  endDate: Date,
  meta: {
    created_date: {
      type: Date,
      default: Date.now
    }
  }
})

// return an array of Pollies whose current party is this one
PartySchema.virtual('members').get(async function() {
  var result

  await PolicyModel.find()
    .where('currentParty').equals(this._id)
    .exec(function(err, res) {
      if (err) return err
      result = res
    })

  return result
})

module.exports = mongoose.model('Party', PartySchema)
